# -*- coding: utf-8 -*-
'''Appendix generator for B180 sTGC wedge integration checklists, using
ReportLab
This file invokes either the GUI or the CLI; if you want to generate 
appendices with your own scripts, call `src.report_generator.make_appendix`
'''

from sys import argv
import argparse

from PyQt5.QtWidgets import QMainWindow, QApplication

from src.report_generator import make_appendix, registered_appendices
from src import argspec
from src.report_generator_widget import ReportGenerator

def get_ui_parser():
    '''Get the parser that determines if we want GUI or CLI'''
    ui_parser = argparse.ArgumentParser(fromfile_prefix_chars='@')
    ui_parser.add_argument('--nolog', action='store_true',
                           help='Do not record build arguments to <path>.log')
    ui_parser.add_argument('--nogui', action='store_true',
                           help='Invoke the CLI')
    ui_parser.add_argument('--cli_help', action='store_true',
                           help='Show the CLI help')
    ui_parser.add_argument('--appendix_help', metavar='N', type=int,
                           help='Show help for appendix N')
    ui_parser.add_argument('--debug', action='store_true',
                           help=argparse.SUPPRESS)
    return ui_parser

def get_gen_parser(appendices):
    '''Get the parser that gets general arguments to make_appendix'''
    gen_parser = argparse.ArgumentParser(prog='<args...>', add_help=False)
    gen_parser.add_argument('path',
                            help='The output report file name')
    gen_parser.add_argument('wedge',
                            help='The wedge MTF ID')
    gen_parser.add_argument('appendix', type=int, choices=appendices,
                            help='The appendix number')
    return gen_parser

def get_apx_parser(appendices, appendix):
    '''Get the parser that gets appendix-specific arguments to 
    make_appendix
    '''
    spec = appendices[appendix][1]
    apx_parser = argspec.to_argparse(**spec)
    apx_parser.prog = '<args...>'
    return apx_parser

def parse_args():
    '''Parse arguments'''
    # See if we want the GUI
    ui_parser = get_ui_parser()
    ui_args, rest = ui_parser.parse_known_args()
    if ui_args.nogui or ui_args.cli_help or ui_args.appendix_help:
        # Get the general arguments to make_appendix
        appendices = registered_appendices()
        gen_parser = get_gen_parser(appendices)
        # Deal with the ugliness that is CLI help
        if ui_args.cli_help:
            gen_parser.print_help()
            gen_parser.exit()
        if ui_args.appendix_help is not None:
            appendix = ui_args.appendix_help
            if appendix not in appendices:
                ui_parser.error(f'No such appendix: {appendix}')
        else:
            gen_args, rest = gen_parser.parse_known_args(rest)
            appendix = gen_args.appendix
        # Get the appendix-specific arguments
        try:
            apx_parser = get_apx_parser(appendices, appendix)
        except TypeError:
            gen_parser.error(f'Appendix {appendix} does not support the CLI')
        if ui_args.appendix_help is not None:
            apx_parser.print_help()
            apx_parser.exit()
        apx_args = apx_parser.parse_args(rest)
        apx_args = argspec.argparse_postprocess(apx_args)
        # Make the appendix
        make_appendix_from_args(ui_args, gen_args, apx_args)
    else:
        if ui_args.debug:
            _debug()
        start_gui(ui_args.nolog)

def make_appendix_from_args(ui_args, gen_args, apx_args):
    '''Make the appendix, and optional log file'''
    make_appendix(gen_args.path, gen_args.appendix, gen_args.wedge,
                  **apx_args)
    if ui_args.nolog:
        return
    args = ['--nogui', gen_args.path, gen_args.wedge, str(gen_args.appendix)]
    args += argspec.argparse_inverse(apx_args)
    with open(gen_args.path + '.log', 'w') as log:
        log.write('\n'.join(args))

def start_gui(nolog):
    '''Start the GUI'''
    app = QApplication(argv)
    w = QMainWindow()
    rg = ReportGenerator(nolog, w)
    w.setCentralWidget(rg)
    w.show()
    app.exec()

def _debug():
    '''Run debugging code'''
    # let's add an appendix 0 to test images
    import src.argspec as asp
    spec = {
        'image_span': asp.FigureFile(),
        'image_restricted': asp.FigureFile(),
        'image_row': asp.VarArgs(asp.FigureFile(), nargs='+'),
    }
    
    def appendix0(
            image_span,
            image_restricted,
            image_row
            ):
        from src.report_generator import figure, mm, textwidth
        row_width = 0.9 * textwidth / len(image_row)
        img_row = [figure(image, width=row_width) for image in image_row]
        return [
            figure(image_span),
            figure(image_restricted, width=40*mm),
            *img_row,
            ]
    
    from src.report_generator import register_appendix
    register_appendix(0, appendix0, spec)

if __name__ == '__main__':
    parse_args()
