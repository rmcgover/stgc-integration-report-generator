# -*- coding: utf-8 -*-
'''Appendix 5: FEB and L1DDC mapping'''

from reportlab.platypus import Paragraph, PageBreak, NextPageTemplate

from .report_generator import CSVTable, styles, tablestyle, register_appendix, first_page_landscape
from . import argspec

class FEBTable(CSVTable):
    def _get_data(self, reader):
        return [row[:5] for row in reader]

class L1DDCTable(CSVTable):
    def _get_data(self, reader):
        data = [row[:4] + row[5:8] for row in reader]
        header = [['STGC L1DDC MAPPING', data[1][0], data[3][0]], []]
        return header + data[5:]

spec = {
    'comments' : argspec.Paragraphs(
        name='Comments',
        help='Comments at the top of the appendix'
        ),
    'febmap' : argspec.CsvFile(
        name='FEB mapping',
        help='The FEB mapping .csv file'
        ),
    'l1ddcmap' : argspec.CsvFile(
        name='L1DDC mapping',
        help='The L1DDC mapping .csv file'
        )
}

def appendix5(comments, febmap, l1ddcmap):
    '''Generatre Appendix 5.
    Arguments:
        comments -- Comments at the top of the appendix
        febmap -- The FEB mapping .csv file
        l1ddcmap -- The L1DDC mapping .csv file
    '''
    first_page_landscape()
    return [
        *(Paragraph(comment, styles['Normal']) for comment in comments),
        L1DDCTable(l1ddcmap, style=tablestyle, repeatRows=1),
        NextPageTemplate('portrait'),
        PageBreak(),
        FEBTable(febmap, style=tablestyle, repeatRows=1),
        ]

register_appendix(5, appendix5, spec)
