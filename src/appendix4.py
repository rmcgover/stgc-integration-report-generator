# -*- coding: utf-8 -*-
'''Appendix 4: mini-SAS cables'''

from reportlab.platypus import Paragraph

from .report_generator import CSVTable, styles, tablestyle, register_appendix
from . import argspec

class ManifestTable(CSVTable):
    def _get_data(self, reader):
        return [row[:4] for row in reader][1:]

#NB: TableStyle is RC, not CR
manifeststyle = tablestyle + (('FONT', (0, 1), (0, -1), 'Courier'),)

spec = {
    'comments' : argspec.Paragraphs(
        name='Comments',
        help='Comments at the top of the appendix'
        ),
    'manifest' : argspec.CsvFile(
        name='Cable bundle manifest',
        help='Cable bundle manifest .csv file'
        )
}

def appendix4(comments, manifest):
    '''Generate Appendix 4.
    Arguments:
        comments -- Comments at the top of the appendix,
        manifest -- The cable bundle manifest .csv file
    '''
    return [
        *(Paragraph(p, styles['Normal']) for p in comments),
        ManifestTable(manifest, style=manifeststyle, repeatRows=1),
        ]

register_appendix(4, appendix4, spec)