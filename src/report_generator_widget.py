# -*- coding: utf-8 -*-
'''The GUI central widget'''

import os.path as osp

from PyQt5.QtCore import QRegularExpression
from PyQt5.QtGui import QRegularExpressionValidator
from PyQt5.QtWidgets import QMessageBox, QPushButton, QScrollArea, QTabWidget, QWidget, QFormLayout, QHBoxLayout, QVBoxLayout, QSizePolicy, QSpacerItem

from .report_generator import registered_appendices, make_appendix
from .argspec import argparse_inverse
from . import argspec_to_qt
from .detailed_dialog import exceptionWarning


class QWedgeValidator(QRegularExpressionValidator):
    '''QValidator for a wedge MTF ID'''
    WEDGE_RE = QRegularExpression('20MNIW[LS][AC][CP][0-9]{5}')
    
    def __init__(self, re=None, parent=None):
        if re is None:
            re = self.WEDGE_RE
        super().__init__(re, parent)

class ReportGenerator(QWidget):
    '''The report generator GUI central widget'''
    
    def __init__(self, nolog=False, parent=None):
        '''Report generator widget constructor.
        Arguments:
            nolog -- whether or not to generate the CLI regeneration log file
            parent -- the Qt parent
        '''
        super().__init__(parent)
        self.nolog = nolog
        self.appendices = []
        layout = QVBoxLayout(self)
        self.setLayout(layout)
        # Due to signals/slots, we assemble the GUI out of order
        # 2. Path and Wedge
        form = QFormLayout(None)
        self.dir = argspec_to_qt.Directory('Appendix output directory', '.', 
                                           self)
        form.addRow('Output directory:', self.dir)
        dir_label = form.labelForField(self.dir)
        dir_label.setToolTip('The output directory')
        self.wedge = argspec_to_qt.Text(self)
        self.wedge.setValidator(QWedgeValidator())
        self.wedge.setPlaceholderText('20MNIWXXX#####')
        form.addRow('Wedge ID:', self.wedge)
        wedge_label = form.labelForField(self.wedge)
        wedge_label.setToolTip('The wedge MTF ID')
        # 1. Appendix tabs
        self.tabs = QTabWidget(self)
        for number, (_, argspec) in registered_appendices().items():
            if argspec is None:
                continue
            sa = QScrollArea(self.tabs)
            appendix = argspec_to_qt.widget(argspec)(sa)
            appendix.directory_changed_tochild.connect(
                self.dir.on_parent_directory_changed)
            sa.setWidget(appendix)
            sa.setWidgetResizable(True)
            self.tabs.addTab(sa, f'Appendix {number}')
            self.appendices.append((number, appendix))
        # 3. Make appendix button
        self.make_appendix_b = QPushButton('Make appendix', self)
        self.make_appendix_b.clicked.connect(self.make_appendix)
        button_layout = QHBoxLayout(None)
        button_layout.addItem(QSpacerItem(0, 0, QSizePolicy.Expanding))
        button_layout.addWidget(self.make_appendix_b)
        # Let's put things in order
        layout.addWidget(self.tabs)
        layout.addLayout(form)
        layout.addLayout(button_layout)
    
    def make_appendix(self):
        '''Gather arguments from current appendix form and create the appendix
        (and possibly regeneration arguments)
        '''
        number, appendix = self.appendices[self.tabs.currentIndex()]
        directory = self.dir.arg_value()
        wedge = self.wedge.arg_value()
        if not directory:
            msg = 'The output directory must be specified'
            QMessageBox.warning(self, 'Error', msg)
            return
        if not wedge:
            msg = 'The wedge MTF ID must be specified'
            QMessageBox.warning(self, 'Error', msg)
            return
        args = appendix.elements()
        filename = f'{wedge}_Appendix_{number}.pdf'
        filepath = osp.join(directory, filename)
        try:
            make_appendix(filepath, number, wedge, **args)
        except Exception:
            msg = f'An error occurred while making the appendix'
            exceptionWarning(self, 'Error', msg, '')
            return
        msg = f'Appendix {number} created at {filepath}'
        QMessageBox.information(self, 'Appendix creation complete', msg)
        if self.nolog:
            return
        logpath = osp.join(directory, filename + '.log')
        rebuild_args = ['--nogui', filepath, wedge, str(number)]
        rebuild_args += argparse_inverse(args)
        with open(logpath, 'w') as log:
            log.write('\n'.join(rebuild_args))

if __name__ == '__main__':
    def test_appendix():
        '''Register a test appendix'''
        from . import argspec as asp
        from .report_generator import register_appendix
        def factory(**kwargs):
            print(kwargs)
            return ()
        argspec = {
            'text' : asp.Text(
                name='Text'),
            'csvfile' : asp.CsvFile(
                name='CsvFile'),
            'figurefile' : asp.FigureFile(
                name='FigureFile'),
            'choices' : asp.Choices(
                choices=('1', '2', '3'),
                name='Choices'),
            'toggle' : asp.Toggle(
                name='Toggle'),
            'paragraphs' : asp.Paragraphs(
                name='Paragraphs'),
            'toggle3' : asp.VarArgs(asp.Toggle(), 3,
                name='3 Toggles'),
            'csv3' : asp.VarArgs(asp.CsvFile(), 3,
                name='3 CsvFiles'),
            'csvqu' : asp.VarArgs(asp.CsvFile(), '?',
                name='Optional CsvFile'),
            'csvst' : asp.VarArgs(asp.CsvFile(), '*',
                name='0 or more CsvFiles'),
            'csvpl' : asp.VarArgs(asp.CsvFile(), '+',
                name='1 or more CsvFiles'),
            'varvar' : asp.VarArgs(asp.VarArgs(asp.CsvFile(), '*'), '*',
                name='a VarArgs of VarArgs'),
            'csvs' : asp.Files(asp.CsvFile(),
                name='same-directory CsvFiles'),
                   }
        register_appendix(0, factory, argspec)
    
    def main():
        '''Display the widget'''
        from sys import argv
        from PyQt5.QtCore import Qt
        from PyQt5.QtWidgets import QApplication
        app = QApplication(argv)
        rg = ReportGenerator()
        rg.setAttribute(Qt.WA_DeleteOnClose)
        rg.show()
        app.exec()
    
    test_appendix()
    main()
