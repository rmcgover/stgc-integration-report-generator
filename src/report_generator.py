# -*- coding: utf-8 -*-
'''Driver code to actually build appendix documents
For end users writing generation scripts: call `make_appendix`
For end users writing appendix modules of their own: this module contains 
    several useful flowables that you can use. Once your appendix is specified,
    call `register_appendix` at the end of your module, and add your module 
    name to `__init__.py`
'''

import csv
import os.path as osp
from importlib import import_module
from collections import namedtuple

from reportlab.pdfgen import canvas
from reportlab.platypus import BaseDocTemplate, PageTemplate, Frame, Flowable, Image, Table
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import mm
import reportlab.lib.enums as rle
from reportlab.lib.utils import ImageReader

from pdfrw import PdfReader
from pdfrw.buildxobj import pagexobj
from pdfrw.toreportlab import makerl

from . import _appendices

### Initialization code

_initialized = False
_APPENDICES = {}

def _init():
    global _initialized
    if _initialized:
        return
    for appendix in _appendices:
        import_module(f'src.{appendix}')
    _initialized = True

Appendix = namedtuple('Appendix', ('factory', 'argspec'))

def register_appendix(number, factory, argspec=None):
    '''Register an appendix.
    Arguments:
        number -- The appendix number
        factory -- A function which takes keyword arguments (optionally 
                   specified by argspec), and returns a list of Flowables
        argspec -- An argument specification dictionary (see `argspec.py`)
    Note to end users: if you're creating your own appendix but only want to
        build it via script, `argspec` is unnecessary. However, the CLI and 
        GUI require an argument specification dictionary using specifiers 
        from `argspec.py`
    '''
    _APPENDICES[number] = Appendix(factory, argspec)

def registered_appendices():
    '''Obtain the dictionary of registered appendices'''
    if not _initialized:
        _init()
    return _APPENDICES

### Layout classes

class Margin:
    '''Convenience class to store useful margin measurements'''
    def __init__(self, size, margin):
        '''Margin constructor
        Arguments:
            size -- a size in points (e.g. from reportlab.lib.pagesizes)
            margin -- a margin width in points
        '''
        width, height = size
        self.pagesize = size
        self.margin = margin
        
        margin_args = ('topMargin', 'leftMargin', 'rightMargin', 'bottomMargin')
        self.margins = {m : margin for m in margin_args}
        
        self.bl = (margin, margin)
        self.br = (width - margin, margin)
        self.tl = (margin, height - margin)
        self.tr = (width - margin, height - margin)

HeaderMarginPortrait = Margin(A4, 12.5 * mm)
HeaderMarginLandscape = Margin(A4[::-1], 12.5 * mm)
BodyMargin = Margin(A4, 25 * mm)

class HeaderCanvas(canvas.Canvas):
    '''reportlab Canvas that stamps a header onto each page. Due to the API
    taking a class rather than a class instance, set the class variables 
    before calling `build`
    Adapted from https://stackoverflow.com/a/28283732/10601881
    '''
    
    APPENDIX = None
    WEDGE = None
    
    def showPage(self):
        self.draw_canvas()
        super().showPage()
    
    @property
    def wedge(self):
        if self.WEDGE is None:
            raise RuntimeError('"WEDGE" was not initialized')
        return self.WEDGE
    
    @property
    def appendix(self):
        if self.APPENDIX is None:
            raise RuntimeError('"APPENDIX" was not initialized')
        return f'Appendix {self.APPENDIX}'
    
    def draw_canvas(self):
        '''This is what actually does the header-ing'''
        self.saveState()
        fontsize = 10
        self.setFont('Times-Roman', fontsize)
        if self._pagesize[0] < self._pagesize[1]:
            HeaderMargin = HeaderMarginPortrait
        else:
            HeaderMargin = HeaderMarginLandscape
        xl, _ = HeaderMargin.tl
        xr, y = HeaderMargin.tr
        y -= fontsize
        self.drawString(xl, y, self.wedge)
        self.drawRightString(xr, y, self.appendix)
        y -= fontsize
        self.setStrokeColorRGB(0, 0, 0)
        self.setLineWidth(0.5)
        self.line(xl, y, xr, y)
        self.restoreState()

### Flowables

class RasterFigure(Image):
    '''reportlab Image with a sensible default size
    Adapted from https://stackoverflow.com/a/5328770/10601881
    '''
    
    def __init__(self, filename, width=None, height=None, *args, **kwargs):
        if None in (width, height):
            img = ImageReader(filename)
            img_width, img_height = img.getSize()
            aspect = img_height / img_width
            if width is not None and height is None:
                height = aspect * width
            elif width is None and height is not None:
                width = height / aspect
            else:
                # TODO: this breaks in landscape
                # TODO: this breaks for tall images
                width = A4[0] - 2 * BodyMargin.margin
                height = aspect * width
        super().__init__(filename, width, height, *args, **kwargs)

class PdfFigure(Flowable):
    '''reportlab Flowable loaded from a PDF.
    Adapted from https://stackoverflow.com/a/32021013/10601881
    '''
    def __init__(self, filename, page=0, width=None, height=None):
        self.img_data = pagexobj(PdfReader(filename).pages[page])
        if None in (width, height):
            img_width, img_height = self.img_data.BBox[2:]
            aspect = img_height / img_width
            if width is not None and height is None:
                height = aspect * width
            elif width is None and height is not None:
                width = height / aspect
            else:
                # TODO: this breaks in landscape
                # TODO: this breaks for tall images
                width = A4[0] - 2 * BodyMargin.margin
                height = aspect * width
        self.img_width = width
        self.img_height = height
    
    def wrap(self, width, height):
        return self.img_width, self.img_height
    
    def drawOn(self, canv, x, y, _sW=0):
        # The parent class tells me not to override this, oops
        if _sW > 0 and hasattr(self, 'hAlign'):
            a = self.hAlign
            if a in ('CENTER', 'CENTRE', rle.TA_CENTER):
                x += 0.5*_sW
            elif a in ('RIGHT', rle.TA_RIGHT):
                x += _sW
            elif a not in ('LEFT', rle.TA_LEFT):
                raise ValueError("Bad hAlign value " + str(a))
        canv.saveState()
        img = self.img_data
        xscale = self.img_width / img.BBox[2]
        yscale = self.img_height / img.BBox[3]
        canv.translate(x, y)
        canv.scale(xscale, yscale)
        canv.doForm(makerl(canv, img))
        canv.restoreState()

def figure(filename, width=None, height=None):
    '''Dispatches between PdfFigure and RasterFigure'''
    _, ext = osp.splitext(filename)
    if 'pdf' in ext:
        figure = PdfFigure(filename, width=width, height=height)
    else:
        figure = RasterFigure(filename, width=width, height=height)
    figure.hAlign = 'CENTER'
    return figure

class CSVTable(Table):
    '''reportlab Table that sources its data from a csv file'''
    
    def __init__(self, path, *args, **kwargs):
        '''Get data from the file located at the `path` argument'''
        try:
            with open(path, newline='') as datafile:
                reader = csv.reader(datafile)
                data = self._get_data(reader)
        except TypeError:
            # Reportlab is doing some magic and rebuilding the table
            # and we're in trouble for changing the API
            data = path
        super().__init__(data, *args, **kwargs)
    
    def _get_data(self, reader):
        '''Use the csv.reader to get the data for initialization.
        Override this if data processing is necessary
        '''
        return [row for row in reader]

### Useful style constants

styles = getSampleStyleSheet() # use this to style Paragraphs

tablestyle = (('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
              ('BOX', (0,0), (-1,-1), 0.25, colors.black)
              )

textwidth = A4[0] - 2 * BodyMargin.margin
textwidthlandscape = A4[1] - 2 * BodyMargin.margin

### Document build script (call this one)

def make_appendix(path, appendix, wedge, **elements):
    '''Make an sTGC wedge checklist appendix
    Arguments:
        path -- The appendix output path
        appendix -- The appendix number
        wedge -- the wedge MTF ID
        elements -- arguments to the appendix factory
    '''
    HeaderCanvas.APPENDIX = appendix
    HeaderCanvas.WEDGE = wedge
    
    if appendix not in _APPENDICES:
        _init()
    try:
        flowables = _APPENDICES[appendix].factory(**elements)
    except KeyError:
        raise RuntimeError(f'No such appendix "{appendix}"')
    
    global _FIRST_PAGE_LANDSCAPE
    doc = BaseDocTemplate(path, pagesize=A4, **BodyMargin.margins)
    portrait = Frame(doc.leftMargin, doc.bottomMargin, doc.width, doc.height)
    landscape = Frame(doc.leftMargin, doc.bottomMargin, doc.height, doc.width)
    templates = [
        PageTemplate(id='portrait', frames=portrait),
        PageTemplate(id='landscape', frames=landscape, pagesize=A4[::-1]) 
    ]
    doc.addPageTemplates(templates if not _FIRST_PAGE_LANDSCAPE
                         else templates[::-1])
    doc.multiBuild(flowables, canvasmaker=HeaderCanvas)
    _FIRST_PAGE_LANDSCAPE = False

_FIRST_PAGE_LANDSCAPE = False

def first_page_landscape():
    '''Call this during your factory function to force the first page to be
    landscape
    '''
    global _FIRST_PAGE_LANDSCAPE
    _FIRST_PAGE_LANDSCAPE = True
