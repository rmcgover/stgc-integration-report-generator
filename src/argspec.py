# -*- coding: utf-8 -*-
'''The argument specification language for appendices. The `argspec` in 
`report_generator.register_appendix` should be a dictionary mapping keyword
argument names of your factory function to classes from this module.
'''

from typing import Optional as Opt, Sequence, Union
from ast import literal_eval

class Arg:
    '''Argument abstract base class'''
    def __init__(self, type: type, name: Opt[str] = None, 
                 help: Opt[str] = None):
        '''Argument specifier
        Arguments:
            type -- The type of the argument
            name -- A human-readable name
            help -- A description of the argument
        '''
        self.type = type
        self.name = name or ''
        self.help = help or ''

class Text(Arg):
    '''Specify that this argument should be a string for display'''
    def __init__(self, name: Opt[str] = None, help: Opt[str] = None):
        type = str
        help = help or 'A string for display'
        super().__init__(type, name, help)

class File(Arg):
    '''Specify that this argument should be a file path conforming to one of
    the given filters
    '''
    def __init__(self, filters: Sequence[str], name: Opt[str] = None, 
                 help: Opt[str] = None):
        '''File argument specifier
        Arguments:
            filters -- A sequence of file filters
            name -- A human-readable name
            help -- A description of the argument
        '''
        self.filters = filters
        type = str
        help = help or f'A file path matching {filters}'
        super().__init__(type, name, help)
    
    def qt_filters(self, all_files=True):
        '''Translate the filters into a Qt file filter string'''
        if all_files:
            filters = tuple(self.filters)
            filters += ('All files (*)',)
        else:
            filters = self.filters
        return ';;'.join(filters)

class CsvFile(File):
    '''Specify that this argument should be a .csv file'''
    def __init__(self, name: Opt[str] = None, help: Opt[str] = None):
        '''A .csv file
        Arguments:
            name -- A human-readable name
            help -- A description of the argument
        '''
        filters = ('Comma-separated values (*.csv)',)
        help = help or 'A .csv file'
        super().__init__(filters, name, help)

class FigureFile(File):
    '''Specify that this argument should be a figure for display'''
    def __init__(self, name: Opt[str] = None, help: Opt[str] = None):
        '''A figure for display, either PDF or a supported image format
        Arguments:
            name -- A human-readable name
            help -- A description of the argument
        '''
        filters = ('Portable document format (*.pdf)',
                   'Image files (*.bmp *.gif *.jpeg *.png)',
                   )
        help = help or 'A figure (PDF or supported image format)'
        super().__init__(filters, name, help)

class Directory(Arg):
    '''Specify that this argument should be a directory'''
    def __init__(self, name: Opt[str] = None, help: Opt[str] = None):
        '''A directory
        Arguments:
            name -- A human-readable name
            help -- A description of the argument
        '''
        type = str
        help = help or 'A directory'
        super().__init__(type, name, help)

class Choices(Arg):
    '''Specify that this argument should be one of a set of choices'''
    def __init__(self, choices: Sequence[str], name: Opt[str] = None,
                 help: Opt[str] = None):
        '''A choice from a sequence
        Arguments:
            choices -- A sequence of strings to choose from
            name -- A human-readable name
            help -- A description of the argument
        '''
        self.choices = choices
        type_ = type(choices[0])
        help = help or f'A choice from {choices}'
        super().__init__(type_, name, help)

class Toggle(Arg):
    '''Specify that this argument should be either true or false'''
    def __init__(self, name: Opt[str] = None, help: Opt[str] = None):
        '''Either true or false
        Arguments:
            name -- A human-readable name
            help -- A description of the argument
        '''
        type = bool
        help = help or 'Either true or false'
        super().__init__(type, name, help)

#TODO: add an Int and a Float?

#Nargs = Union[int, Literal['?', '*', '+']] # not until python 3.8 :c
Nargs = Union[int, str]
class VarArgs(Arg):
    '''Specify a variable number of arguments of the same type'''
    
    NARGSPEC = ('?', '*', '+')
    
    _NARGHELP = {
        '?' : '0 or 1 of: ',
        '*' : '0 or more of: ',
        '+' : '1 or more of: ',
    }
    
    def __init__(self, value: Arg, nargs: Nargs, name: Opt[str] = None,
                 help: Opt[str] = None):
        '''Variable number of arguments of type `value`
        Arguments:
            value -- An `Arg`, whose type is inherited
            nargs -- Follows the argparse add_argument specification, i.e.
                     an int or one of '?', '*', or '+'
            name -- A human-readable name
            help -- A description of the argument
        '''
        self.value = value
        type = value.type
        if nargs in self.NARGSPEC:
            self.nargs = nargs
            if help is None and value.help:
                help = self._NARGHELP[nargs] + value.help
        elif isinstance(nargs, int):
            self.nargs = nargs
            if help is None and value.help:
                help = f'Exactly {nargs} of: ' + value.help
        else:
            raise ValueError(nargs)
        super().__init__(type, name, help)

class Paragraphs(VarArgs):
    '''Specify that this argument should be a (possibly empty) tuple of
    strings to be processed as reportlab Paragraphs
    '''
    def __init__(self, name: Opt[str] = None, help: Opt[str] = None):
        '''A tuple of strings
        Arguments:
            name -- A human-readable name
            help -- A description of the argument
        '''
        value = Text()
        nargs = '*'
        help = help or 'A tuple of strings'
        super().__init__(value, nargs, name, help)

class Files(VarArgs):
    '''Specify that this argument should be one or more files in the same
    directory
    '''
    def __init__(self, value: Opt[File] = None, name: Opt[str] = None, 
                 help: Opt[str] = None):
        '''One or more files of type `value` in the same directory
        Arguments:
            value -- a specified `File` argument
            name -- A human-readable name
            help -- A description of the argument
        '''
        value = value or File()
        nargs = '+'
        help = help or 'A tuple of one or more files in the same directory'
        super().__init__(value, nargs, name, help)

def to_argparse(**argspec):
    '''Convert keyword arguments to an argparse.ArgumentParser'''
    from argparse import ArgumentParser
    parser = ArgumentParser(add_help=False)
    for name, arg in argspec.items():
        if isinstance(arg, VarArgs):
            nargs = arg.nargs
        else:
            nargs = None
        if isinstance(arg, Choices):
            choices = arg.choices
        else:
            choices = None
        if arg.type == bool:
            type = lambda b: b == 'True'
        elif isinstance(arg, VarArgs) and isinstance(arg.value, VarArgs):
            type = literal_eval
        else:
            type = arg.type
        parser.add_argument(f'--{name}', nargs=nargs, choices=choices,
                            type=type, required=True, help=arg.help)
    return parser

def argparse_postprocess(args):
    '''Postprocess results of parse_arguments'''
    return {k : tuple(v) if isinstance(v, list) else () if v is None else v
            for k, v in vars(args).items()}

def argparse_inverse(args):
    '''Invert the mapping between argparse arguments and postprocessed 
    arguments
    '''
    inv = []
    for name, arg in args.items():
        inv.append(f'--{name}')
        if isinstance(arg, tuple):
            inv += list(str(a) for a in arg)
        else:
            inv.append(str(arg))
    return inv        

if __name__ == '__main__':
    def main():
        '''Test case for argspec <==> argparse round-trip'''
        from argparse import ArgumentParser
        argspec = {'a' : Text(),
                   'b' : CsvFile(),
                   'c' : FigureFile(name='not c', help='c!'),
                   'd' : Choices(('1', '2', '3')),
                   'e' : Toggle(),
                   'f' : Paragraphs(),
                   'g' : VarArgs(Toggle(), 3),
                   'h' : VarArgs(CsvFile(), 3),
                   'i' : VarArgs(CsvFile(), '?'),
                   'j' : VarArgs(CsvFile(), '*'),
                   'k' : VarArgs(CsvFile(), '+'),
                   'l' : VarArgs(VarArgs(CsvFile(), '*'), '*'),
                   'm' : Directory(),
                   'n' : Files(CsvFile()),
                   }
        args = {
            'a' : 'Text',
            'b' : 'fake.csv',
            'c' : 'fake.pdf',
            'd' : '2',
            'e' : True,
            'f' : ('two', 'lines'),
            'g' : (True, False, True),
            'h' : ('fake1.csv', 'fake2.csv', 'fake3.csv'),
            'i' : (),
            'j' : (),
            'k' : ('fake.csv',),
            'l' : ((), ('a.csv', 'b.csv'), ('c.csv',)),
            'm' : '.',
            'n' : ('fake1.txt', 'fake2.txt',),
            }
        inv = argparse_inverse(args)
        parser = to_argparse(**argspec)
        parsed = parser.parse_args(inv)
        processed = argparse_postprocess(parsed)
        assert args == processed
        with open('test/argtest.txt', 'w') as file:
            file.write('\n'.join(inv))
        dummy = ArgumentParser(fromfile_prefix_chars='@', add_help=False)
        _, rest = dummy.parse_known_args(['@test/argtest.txt'])
        assert rest == inv
        read = parser.parse_args(rest)
        read_processed = argparse_postprocess(read)
        assert args == read_processed
    
    main()
