# -*- coding: utf-8 -*-
'''Appendix 6: Current and Temperature Monitoring'''

import csv

from reportlab.platypus import PageBreak, Paragraph, Table

from .report_generator import CSVTable, figure, styles, tablestyle, \
    register_appendix
from . import argspec

class CurrentStatsTable(CSVTable):
    '''Eliminates the LVPS IP address in the header'''
    def _get_data(self, reader):
        data = [row for row in reader]
        headers = data[0][1:]
        for i, header in enumerate(headers):
            try:
                ip, name = header.split(':')
                headers[i] = name
            except ValueError:
                pass
        data[0] = [data[0][0]] + headers
        return data

def get_temperature_stats_tables(path, *args, **kwargs):
    '''Create two `CSVTable`s from the `path` argument, forwarding all other
    argments
    '''
    with open(path, newline='') as datafile:
        reader = csv.reader(datafile)
        data = [row for row in reader]
    i = data.index([])
    board_stats, vmm_stats = data[:i], data[i+1:]
    board_table = Table(board_stats, *args, **kwargs)
    vmm_table = Table(vmm_stats, *args, **kwargs)
    return board_table, vmm_table

spec = {
        'comments' : argspec.Paragraphs(
            name='Comments',
            help='Comments at the top of the appendix'),
        'current_plot' : argspec.FigureFile(
            name='Current plot',
            help='The path to the current plot'),
        'current_stats' : argspec.CsvFile(
            name='Current stats',
            help='The path to the current statistics csv'),
        'temperature_plot' : argspec.FigureFile(
            name='Temperature plot',
            help='The path to the temperature plot'),
        'temperature_stats' : argspec.CsvFile(
            name='Temperature overview',
            help='The path to the temperature overview csv'),
        }

def appendix6(**elements):
    '''Generate Appendix 6.
    Arguments:
        comments -- Comments at the top of the appendix
        current_plot -- The path to the current plot
        current_stats -- The path to the current statistics csv
        temperature_plot -- The path to the temperature plot
        temperature_stats -- The path to the temperature overview csv
    '''
    flowables = []
    comments = (Paragraph(p, styles['Normal']) for p in elements['comments'])
    curr_plot = figure(elements['current_plot'])
    curr_stats = CurrentStatsTable(elements['current_stats'], style=tablestyle)
    temp_plot = figure(elements['temperature_plot'])
    board_stats, vmm_stats = get_temperature_stats_tables(elements['temperature_stats'], style=tablestyle)
    
    flowables.extend((
        *comments,
        curr_plot,
        curr_stats,
        PageBreak(),
        temp_plot,
        board_stats,
        PageBreak(),
        vmm_stats,
    ))
    
    return flowables

register_appendix(6, appendix6, spec)
