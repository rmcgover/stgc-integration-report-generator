# -*- coding: utf-8 -*-
'''Converts an argument specification dictionary into a Qt form widget
End user: this isn't useful to you, I promise.
'''

import os.path as osp

from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt
from PyQt5.QtWidgets import QButtonGroup, QCheckBox, QFileDialog, QLabel, QLineEdit, QListWidget, QPushButton, QRadioButton, QTextEdit, QWidget, QFormLayout, QGridLayout, QHBoxLayout, QVBoxLayout, QSizePolicy, QSpacerItem, QStyle

from . import argspec as asp

class Text(QLineEdit):
    '''Text entry'''
    
    def arg_value(self):
        '''The argument value'''
        return self.text()

class Paragraphs(QTextEdit):
    '''Paragraphs entry'''
    
    def arg_value(self):
        '''The argument value'''
        return tuple(self.toPlainText().split('\n'))

class File(QWidget):
    '''File entry'''
    
    def __init__(self, filters, description, directory=None, parent=None):
        '''File entry constructor
        Arguments:
            filters -- the Qt file filter string
            description -- the title to show in the file dialog
            directory -- the initial directory (optional)
            parent -- the Qt parent (optional)
        '''
        super().__init__(parent)
        self.filters = filters
        self.description = description
        self.directory = directory or '.'
        layout = QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)
        # see https://doc.qt.io/qt-5/qstyle.html#StandardPixmap-enum
        icon = self.style().standardIcon(QStyle.SP_DirOpenIcon)
        self.open_file_b = QPushButton(icon, '', self)
        self.open_file_b.clicked.connect(self.open_file)
        layout.addWidget(self.open_file_b)
        self.path = QLineEdit(self)
        self.path.setReadOnly(True)
        layout.addWidget(self.path)
    
    def open_file(self):
        title = f'Open file - {self.description}'
        path, _ = QFileDialog.getOpenFileName(self, title, 
                                              self.directory, self.filters)
        if not path:
            self.path.clear()
            return
        self.path.setText(path)
        self.directory = osp.dirname(path)
        self.directory_changed_toparent.emit(self.directory, id(self))
    
    def arg_value(self):
        '''The argument value'''
        return self.path.text()
    
    # directory change protocol: always the child
    
    directory_changed_toparent = pyqtSignal(str, int)
    
    @pyqtSlot(str, int)
    def on_parent_directory_changed(self, directory, sigid):
        if sigid == id(self):
            return
        self.directory = directory

class Directory(File):
    '''Directory entry'''
    
    def __init__(self, description, directory=None, parent=None):
        '''Directory entry constructor
        Arguments:
            description -- the title to show in the directory dialog
            directory -- the initial directory (optional)
            parent -- the Qt parent (optional)
        '''
        super().__init__('', description, directory, parent)
    
    def open_file(self):
        title = f'Open directory - {self.description}'
        path = QFileDialog.getExistingDirectory(self, title, self.directory)
        if not path:
            return
        self.path.setText(path)
        self.directory = osp.dirname(path) # the parent directory
        self.directory_changed_toparent.emit(self.directory, id(self))

class Files(QWidget):
    '''Files entry'''
    
    def __init__(self, filters, description, directory=None, parent=None):
        '''File entry constructor
        Arguments:
            filters -- the Qt file filter string
            description -- the title to show in the file dialog
            directory -- the initial directory (optional)
            parent -- the Qt parent (optional)
        '''
        super().__init__(parent)
        self.filters = filters
        self.description = description
        self.directory = directory or '.'
        layout = QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)
        # see https://doc.qt.io/qt-5/qstyle.html#StandardPixmap-enum
        icon = self.style().standardIcon(QStyle.SP_DirOpenIcon)
        self.open_file_b = QPushButton(icon, '', self)
        self.open_file_b.clicked.connect(self.open_file)
        layout.addWidget(self.open_file_b)
        layout.setAlignment(self.open_file_b, Qt.AlignTop)
        self.paths = QListWidget(self)
        self.paths.setSelectionMode(QListWidget.NoSelection)
        layout.addWidget(self.paths)
    
    def open_file(self):
        title = f'Open files - {self.description}'
        paths, _ = QFileDialog.getOpenFileNames(self, title,
                                                self.directory, self.filters)
        self.paths.clear()
        if not paths:
            return
        self.paths.addItems(paths)
        self.directory = osp.dirname(paths[0])
        self.directory_changed_toparent.emit(self.directory, id(self))
    
    def arg_value(self):
        '''The argument value'''
        return tuple(self.paths.item(i).text() 
                     for i in range(self.paths.count()))
    
    # directory change protocol: always the child
    
    directory_changed_toparent = pyqtSignal(str, int)
    
    @pyqtSlot(str, int)
    def on_parent_directory_changed(self, directory, sigid):
        if sigid == id(self):
            return
        self.directory = directory

class Choices(QWidget):
    '''Choices entry'''
    
    def __init__(self, choices, parent=None):
        super().__init__(parent)
        self.choices = choices
        layout = QGridLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)
        self.buttongroup = QButtonGroup()
        for i, choice in enumerate(self.choices):
            layout.addWidget(QLabel(choice, self), 0, i, Qt.AlignHCenter)
            button = QRadioButton(self)
            self.buttongroup.addButton(button, i)
            layout.addWidget(button, 1, i, Qt.AlignHCenter)
        self.buttongroup.button(0).setChecked(True)
        spacer = QSpacerItem(0, 0, QSizePolicy.Expanding)
        layout.addItem(spacer, 0, len(self.choices), 2, -1)
        
    def arg_value(self):
        '''The argument value'''
        return self.choices[self.buttongroup.checkedId()]

class Toggle(QCheckBox):
    '''Toggle entry'''
    
    def arg_value(self):
        '''The argument value'''
        return self.isChecked()

class VarArgs(QWidget):
    '''Abstract base class for VarArgs entry'''
    
    def __init__(self, value, nargs, parent=None):
        '''VarArgs constructor
        Arguments:
            value -- the VarArgs.value member
            nargs -- the VarArgs.nargs member
            parent -- the Qt parent (optional)
        '''
        super().__init__(parent)
        self.value = value
        self.nargs = nargs
        self.varargs = []
    
    def arg_value(self):
        '''The argument value'''
        return tuple(w.arg_value() for w in self.varargs)
    
    # directory change protocol: can be both parent and child
    
    directory_changed_toparent = pyqtSignal(str, int)
    directory_changed_tochild = pyqtSignal(str, int)
    
    @pyqtSlot(str, int)
    def on_parent_directory_changed(self, directory, sigid):
        if sigid == id(self):
            return
        self.directory_changed_tochild.emit(directory, sigid)
    
    @pyqtSlot(str, int)
    def on_child_directory_changed(self, directory, sigid):
        self.directory_changed_tochild.emit(directory, sigid)
        self.directory_changed_toparent.emit(directory, id(self))

class VarArgsNum(VarArgs):
    '''VarArgs implementation for fixed number of args'''
    
    def __init__(self, value, nargs, parent=None):
        super().__init__(value, nargs, parent)
        layout = QVBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)
        for i in range(nargs):
            w = widgetforarg(self.value, self)
            self.varargs.append(w)
            layout.addWidget(w)

class VarArgsQuestion(VarArgs):
    '''VarArgs implementation for "?"'''
    
    def __init__(self, value, nargs='?', parent=None):
        super().__init__(value, nargs, parent)
        layout = QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)
        self.enable_b = QCheckBox(self)
        layout.addWidget(self.enable_b)
        w = widgetforarg(self.value, self)
        self.varargs.append(w)
        layout.addWidget(w)
        self.enable_b.toggled.connect(w.setEnabled)
        self.enable_b.setChecked(False)
        w.setEnabled(False) # in theory I shouldn't have to call this
    
    def arg_value(self):
        '''The argument value'''
        if self.enable_b.isChecked():
            return super().arg_value()
        else:
            return ()

class VarArgsButton(QWidget):
    '''Button for adding and removing rows of VarArgsStarPlus; has two
    QPushButtons, `plus` and `minus` which display dynamically based on 
    the number of rows and their index
    '''
    
    def __init__(self, nargs, index, parent=None):
        '''Constructor
        Arguments:
            nargs -- the VarArgs mode
            index -- the row of this button
            parent -- the Qt parent (optional)
        '''
        super().__init__(parent)
        self.nargs = nargs
        self.index = index
        layout = QHBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)
        self.plus = QPushButton('+', self)
        self.minus = QPushButton('−', self) # U+2212
        self.plus.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.plus.setFixedWidth(self.plus.height())
        self.minus.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.minus.setFixedWidth(self.minus.height())
        layout.addWidget(self.plus)
        layout.addWidget(self.minus)
    
    @pyqtSlot(int)
    def on_nrows_changed(self, nrows):
        '''Slot for number of VarArgs rows changed'''
        self.minus.setEnabled(True)
        if nrows > self.index + 1:
            # full row that is not the last
            self.plus.hide()
            self.minus.show()
            self.minus.setEnabled(False)
        elif nrows == self.index + 1:
            # full row that is the last
            self.plus.hide()
            self.minus.show()
            if self.nargs == '+' and self.index == 0:
                self.minus.setEnabled(False)
        elif nrows == self.index:
            # one past the last row
            self.plus.show()
            self.minus.hide()
        else: # nrows < self.index
            # this should never happen
            pass

class VarArgsStarPlus(VarArgs):
    '''VarArgs implementation for nargs in ('*', '+')'''
    
    def __init__(self, value, nargs, parent=None):
        super().__init__(value, nargs, parent)
        self.buttons = []
        layout = QGridLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)
        self.add_button(0)
        if self.nargs == '+':
            self.add_widget(0)
            self.add_button(1)
        else:
            self.spacer = QSpacerItem(0, 0, QSizePolicy.Expanding)
            layout.addItem(self.spacer, 0, 1)
        self.nrows_changed.emit(len(self.varargs))
        
    def add_button(self, index):
        layout = self.layout()
        button = VarArgsButton(self.nargs, index, self)
        button.plus.clicked.connect(self.add_row)
        button.minus.clicked.connect(self.remove_row)
        self.nrows_changed.connect(button.on_nrows_changed)
        self.buttons.append(button)
        layout.addWidget(button, index, 0)
        layout.setAlignment(button, Qt.AlignTop)
    
    def add_widget(self, index):
        layout = self.layout()
        w = widgetforarg(self.value, self)
        self.varargs.append(w)
        layout.addWidget(w, index, 1)
        layout.setAlignment(w, Qt.AlignTop)
    
    @pyqtSlot()
    def add_row(self):
        index = len(self.varargs)
        if index == 0:
            self.layout().removeItem(self.spacer)
        self.add_widget(index)
        self.add_button(index + 1)
        self.nrows_changed.emit(len(self.varargs))
    
    @pyqtSlot()
    def remove_row(self):
        layout = self.layout()
        button = self.buttons.pop()
        w = self.varargs.pop()
        layout.removeWidget(button)
        layout.removeWidget(w)
        button.deleteLater()
        w.deleteLater()
        nrows = len(self.varargs)
        if nrows == 0:
            layout.addItem(self.spacer, 0, 1)
        self.nrows_changed.emit(len(self.varargs))
    
    nrows_changed = pyqtSignal(int) # emitted to buttons

def varargs(nargs):
    '''Dispatch between the various VarArgs classes'''
    if isinstance(nargs, int):
        return VarArgsNum
    if nargs == '?':
        return VarArgsQuestion
    if nargs in ('*', '+'):
        return VarArgsStarPlus

def widgetforarg(arg, parent=None):
    '''Get the Qt widget for the corresponding argument specifier
    Arguments:
        arg -- an argument specifier instance from `argspec.py`
        parent -- the Qt parent (optional)
    '''
    if isinstance(arg, asp.Text):
        return Text(parent)
    elif isinstance(arg, asp.Paragraphs): # special case of VarArgs!
        return Paragraphs(parent)
    elif isinstance(arg, asp.File):
        description = arg.name or arg.help
        w = File(arg.qt_filters(), description, parent=parent)
        if parent is not None:
            w.directory_changed_toparent.connect(parent.on_child_directory_changed)
            parent.directory_changed_tochild.connect(w.on_parent_directory_changed)
        return w
    elif isinstance(arg, asp.Directory):
        description = arg.name or arg.help
        w = Directory(description, parent=parent)
        if parent is not None:
            w.directory_changed_toparent.connect(parent.on_child_directory_changed)
            parent.directory_changed_tochild.connect(w.on_parent_directory_changed)
        return w
    elif isinstance(arg, asp.Files): # special case of VarArgs!
        description = arg.name or arg.help
        w = Files(arg.value.qt_filters(), description, parent=parent)
        if parent is not None:
            w.directory_changed_toparent.connect(parent.on_child_directory_changed)
            parent.directory_changed_tochild.connect(w.on_parent_directory_changed)
        return w
    elif isinstance(arg, asp.Choices):
        return Choices(arg.choices, parent)
    elif isinstance(arg, asp.Toggle):
        return Toggle(parent)
    elif isinstance(arg, asp.VarArgs):
        klass = varargs(arg.nargs)
        w = klass(arg.value, arg.nargs, parent)
        if parent is not None:
            w.directory_changed_toparent.connect(parent.on_child_directory_changed)
            parent.directory_changed_tochild.connect(w.on_parent_directory_changed)
        return w
    else:
        raise NotImplementedError(type(arg))

def widget(argspec):
    '''Return the Qt form widget corresponding to `argspec`
    Arguments:
        argspec -- an argument specification dictionary
    '''
    
    class Appendix(QWidget):
        '''Qt form which takes user input based on an argument specification
        (This widget is dynamically generated)
        '''
        
        ARGSPEC = argspec
        
        def __init__(self, parent = None):
            '''Appendix form widget constructor
            Arguments:
                parent -- the Qt parent
            '''
            super().__init__(parent)
            self.args = []
            layout = QFormLayout(self)
            self.setLayout(layout)
            for name, arg in self.ARGSPEC.items():
                name = (arg.name or name) + ':'
                w = widgetforarg(arg, self)
                self.args.append(w)
                layout.addRow(name, w)
                label = layout.labelForField(w)
                label.setToolTip(arg.help)
        
        def elements(self):
            '''Return the keyword argument dictionary for the appendix
            factory function
            '''
            return {name : arg.arg_value() 
                    for name, arg in zip(self.ARGSPEC, self.args)}
    
        # directory change protocol: this is always the parent
        
        directory_changed_tochild = pyqtSignal(str, int)
        
        @pyqtSlot(str, int)
        def on_child_directory_changed(self, directory, sigid):
            self.directory_changed_tochild.emit(directory, sigid)
    
    return Appendix

if __name__ == '__main__':
    def main():
        '''Test case for argspec conversion to Qt form'''
        from sys import argv
        from PyQt5.QtWidgets import QApplication
        argspec = {'a' : asp.Text(),
                   'b' : asp.CsvFile(),
                   'c' : asp.FigureFile(name='not c', help='c!'),
                   'd' : asp.Choices(('1', '2', '3')),
                   'e' : asp.Toggle(),
                   'f' : asp.Paragraphs(),
                   'g' : asp.VarArgs(asp.Toggle(), 3),
                   'h' : asp.VarArgs(asp.CsvFile(), 3),
                   'i' : asp.VarArgs(asp.CsvFile(), '?'),
                   'j' : asp.VarArgs(asp.CsvFile(), '*'),
                   'k' : asp.VarArgs(asp.CsvFile(), '+'),
                   'l' : asp.VarArgs(asp.VarArgs(asp.CsvFile(), '*'), '*'),
                   'm' : asp.Directory(),
                   'n' : asp.Files(asp.CsvFile()),
                   }
        app = QApplication(argv)
        a = widget(argspec)()
        a.setAttribute(Qt.WA_DeleteOnClose)
        a.show()
        app.exec()
    
    main()