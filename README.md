# NSW Electronics - Appendix generation for B180 sTGC wedge integration checklists

## Table of contents

[[_TOC_]]

## Introduction

This project contains the code necessary to generate the appendices for the Building 180 sTGC wedge integration checklists.

At the moment, the following appendices are implemented:

* Appendix 4: mini-SAS cables
* Appendix 5: FEB and L1DDC mapping
* Appendix 6: Current and Temperature monitoring

However, it is fairly trivial to implement the content of another appendix.

## Dependencies

Dependencies are listed in the [`environment.yml`](environment.yml) file, and can be easily acquired by creating a new [Anaconda](https://www.anaconda.com/) enviroment:
```bash
conda env create -n <env-name> -f environment.yml
conda activate <env-name>
```
or merging with an existing environment:
```bash
conda env update -n <env-name> -f environment.yml
conda activate <env-name>
```

## I want to generate an existing appendix!

Couldn't be easier!

Invoke the GUI:
```bash
python generate_report.py
```

Select your appendix, and input the required fields. If you need help understanding what each field is, you can hover over its name for help text.

Next, choose the output directory and specify the wedge MTF ID. Finally, click "Generate appendix". Two files will be created:

* `<MTF-ID>_Appendix_<number>.pdf`: your new appendix!
* `<MTF-ID>_Appendix_<number>.pdf.log`: a file containing the arguments to regenerate this appendix using the CLI.

This last file can be quite useful. You can regenerate the appendix by simply "splatting" its contents with `@` (quotes are unnecessary on Linux):
```bash
python generate_report.py "@<path to log file>"
```
This is helpful if you want to make an edit to your input data, or your input data changed and you don't want to reenter all the fields into the GUI again.

## I want to create a new appendix, and generate it with a script!

Fairly simple!

Look to [`src/appendix6.py`](src/appendix6.py) as an example. You'll want to make a similar module, `src/appendix<n>.py` (where `<n>` is the number of your appendix).

The core of your module is a *factory function*, which takes keyword arguments as input, and returns a list of ReportLab `Flowable`s (the body of the appendix).

`reportlab.platypus` contains several useful `Flowable`s, such as:

* `Paragraph`: takes in text and a style, and displays that text with newlines stripped
* `Table`: takes in a two-dimensional sequence of data (`str`, `Flowable`, *etc.*) and a style keyword argument, and displays it as a table
* `NextPageTemplate`: forces the next page to use the given template (either `portrait` or `landscape`)
* `Pagebreak`: forces the page to break at this position

See the [ReportLab documentation](https://www.reportlab.com/docs/reportlab-userguide.pdf) for more info on the various `Flowable`s in ReportLab.

The [`src.report_generator`](src/report_generator.py) module contains some useful flowables as well:

* `figure`: actually a function, which takes an input filename which can be a .pdf or any image supported by the [`pillow`](https://pillow.readthedocs.io/en/stable/handbook/image-file-formats.html) package; by default, the figure spans the width of the page, but you can supply a `width` argument
* `CSVTable`: a subclass of ReportLab's `Table` that takes an input .csv file and puts it into a table; if you need to process your data before displaying it, subclass this class and reimplement the `_get_data` method

The [`src.report_generator`](src/report_generator.py) module also contains some useful constants for formatting:

* `styles`: the ReportLab default stylesheet; you can add style to a `Paragraph` by using *e.g.* `Paragraph('text', styles['Normal'])`
* `tablestyle`: a value for the `Table` `style` keyword argument, which has all of the inner and outer gridlines
* `textwidth`, `textwidthlandscape`: like LaTeX's `\textwidth`, useful as an argument to `figure()`

If you need to switch between portrait and landscape mode (*e.g.* for a long table), use the following template:
```python
elements = [
    <portrait content>,
    NextPageTemplate('landscape'),
    PageBreak(),
    <landscape content>,
    NextPageTemplate('portrait'),
    <more portrait content>
]
```
Due to limitations in ReportLab, if you need the first page to be landscape, call the function `first_page_landscape` (imported from [`src.report_generator`](src/report_generator.py)) at the start of your factory function.

Once your factory function is done, there's only two steps left!

First, register your appendix module. At the end of `src/appendix<n>.py`, put the line:
```python
report_generator.register_appendix(<n>, <your factory function>)
```
and add its module name to [`__init__.py`](src/__init__.py):
```python
_appendices = [
    ...
    'appendix<n>',
    ...
]
```

Finally, you're ready to build your appendix! Your script file can look as simple as this:
```python
from src.report_generator import make_appendix
elements = {
    'arg1' : 'val1',
    'arg2' : 'val2',
    ...
}
make_appendix('appendix<n>.pdf', <n>, '20MNIWXXX00000', **elements)
```

That's it!

## I want to create a new appendix, and use it with the GUI/CLI!

Great! Good work taking the initiative.

First, complete the steps from [the above section](#i-want-to-create-a-new-appendix-and-generate-it-with-a-script).

Now, you are going to produce an *argument specification* for your factory function. This is going to use *argument specifiers* from the [`src.argspec`](src/argspec.py) module.

Let's say your factory function looks like this:
```python
def appendix<n>(comments, image_file, csv_file):
    return [
        *(Paragraph(comment, styles['Normal']) for comment in comments),
        figure(image_file),
        CsvTable(csv_file),
    ]
```
where `comments` is a sequence of comment strings, `image_file` is a path to an image file, and `csv_file` is a path to a .csv file.

Then, you would create an argument specifcation dictionary for it that looks like:
```python
spec = {
    'comments' : argspec.Paragraphs(
        name='Comments',
        help='Comments at the top of the appendix'),
    'image_file' : argspec.FigureFile(
        name='Test plot',
        help='The plot file of the test data'),
    'csv_file' : argspec.CsvFile(
        name='Test data',
        help='The test data file'),
}
```
Finally, add this specification to your appendix registration:
```python
report_generator.register_appendix(<n>, appendix<n>, spec)
```
Et voilà! Your appendix is now available to the GUI and CLI.

The `argspec` module supports a fairly comprehensive range of argument types:

* `Text`: a string
* `Paragraphs`: a sequence of strings intended for display
* `File`: a file path (with the option to filter based on filetype)
* `CsvFile`: a .csv file path
* `FigureFile`: an image or PDF file
* `Directory` : a directory
* `Files` : one or more files, all from the same directory
* `Choices`: a single choice from a set of different strings
* `Toggle`: either true or false
* `VarArgs`: some number of a single argument type from above; this can be a specific number, `'?'` for zero or one, `'*'` for zero or more, or `'+'` for one or more

Read the module for further documentation on how to use these specifiers.

To see an example of how these specifiers translate into GUI controls, run
```bash
python -m src.report_generator_widget
```
